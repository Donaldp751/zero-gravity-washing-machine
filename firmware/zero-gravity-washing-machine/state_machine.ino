#include "state_machine.h"
#include "debug.h"

void state_machine_run(){
    static enum states machine_state = STARTUP;
    
    switch (machine_state)
    {
    case STARTUP:
        /* code */
        break;
    case HOMING:
        /* code */
        break;
    case STANDBY:
        /* code */
        break;
    case REMOVING_AIR:
        /* code */
        break;
    case ADDING_WATER:
        /* code */
        break;
    case WASHING:
        /* code */
        break;
    case REMOVING_WATER:
        /* code */
        break;
    case DEPRESSURIZING:
        /* code */
        break; 
    case WASH_COMPLETE:
        /* code */
        break;
    case MACHINE_FAULT:
        /* code */
        break;         
    default:
        /*PROGRAM ERROR*/
        break;
    }
}