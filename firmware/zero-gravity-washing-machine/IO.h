

/*Returns high on start button press*/
char getStartBtnSignal();
/*Returns high on stop button press*/
char getStopBtnSignal();

int getEncoderOnePosition();
int getEncoderTwoPosition();
void encoderOneInterrupt();
void encoderTwoInterrupt();