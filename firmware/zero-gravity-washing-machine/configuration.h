/*
Braham zero gravity washing machine configuration file
2021

*/

#ifndef __CONFIGURATION_H__
#define __CONFIGURATION_H__

#define C_PUMP_ENC_PPMM 27
#define C_WASH_ENC_PPMM 27

/*OUTPUTS*/
#define C_VALVE_1 22
#define C_VALVE_2 23
#define C_VALVE_3 24
#define C_VALVE_4 25
#define C_VALVE_5 26
#define C_VALVE_6 27

/*Wash actuator*/
#define C_PISTON_1_IN_1 5
#define C_PISTON_1_IN_2 9
#define C_PISTON_1_SPEED 6
/*Wash actuator end*/

/*Pump actuator*/
#define C_PISTON_2_IN_1 8
#define C_PISTON_2_IN_2 11
#define C_PISTON_2_SPEED 7
/*Pump actuator end*/

/*END OUTPUTS*/


/*INPUTS*/
#define C_START_BTN 40 
#define C_PAUSE_BTN 41
#define C_STOP_BTN 42

/*Wash actuator encoders*/
#define C_ENC_1_A 2
#define C_ENC_1_B 3
/*Wash actuator encoders end*/

/*Pump actutator encoders*/
#define C_ENC_2_A 18
#define C_ENC_2_B 19
/*Pump actutator encoders end*/

/*END OUTPUTS*/



#endif /* __CONFIGURATION_H__ */