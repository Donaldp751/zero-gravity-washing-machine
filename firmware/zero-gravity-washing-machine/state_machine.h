#ifndef __STATE_MACHINE_H__
#define __STATE_MACHINE_H__

void state_machine_run();

enum states {
  STARTUP,
  HOMING,
  STANDBY,
  REMOVING_AIR,
  ADDING_WATER,
  WASHING,
  REMOVING_WATER,
  DEPRESSURIZING,
  WASH_COMPLETE,
  MACHINE_FAULT
};

#endif