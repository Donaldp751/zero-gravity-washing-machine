
#ifdef SIMULATION 
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "main.h"



static const char *const evval[3] = {
    "RELEASED",
    "PRESSED ",
    "REPEATED"
};

#include "simulated_wrapper.h"

int main(int argc, char **argv){

   const char *dev = "/dev/stdin"; /*standard input to get commands from*/
    struct input_event ev;
    ssize_t n;
    int fd;

    fd = open(dev, O_RDONLY);
    if (fd == -1) {
        fprintf(stderr, "Cannot open %s: %s.\n", dev, strerror(errno));
        return EXIT_FAILURE;
    }
    char read_buffer[1024];
    int error = SUCCESS;
    while(1){
        int bytesRead = read(fd, read_buffer, 10);
        if(bytesRead != -1){
            error = validateCMD(read_buffer);
            if(error == SUCCESS){
                error = setIO();
            }
            if(error == SUCCESS){
                state_machine_run();
            }
            if(error == SUCCESS){
                visalizeOutput();
            }
        }
        printf("looping...\n");
        usleep(1000 * 100);
    }
    return 0;
}

int validateCMD(){
    
    return 0;
}
int setIO(){
    
    return 0;
}
void visalizeOutput(){
    

}

void digitalWrite(uint8_t pin, uint8_t val){
    printf("Pin %d set to %d\n",pin,val);
}

void pinMode(uint8_t pin, uint8_t mode){
    printf("Pin %d mode set to %d\n",pin,mode);  
}
#endif
