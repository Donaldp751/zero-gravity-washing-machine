#ifndef __MAIN_H__
#define __MAIN_H__

#ifdef SIMULATION 
    #include <unistd.h>
    #define DELAY(ms) usleep(ms * 1000)
#else
    #define DELAY(ms) delay(ms)
#endif /*SIMULATION*/


#endif