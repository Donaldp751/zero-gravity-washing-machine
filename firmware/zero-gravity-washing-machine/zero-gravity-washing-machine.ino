/*
Zero gravity washing machine firmware for Braham Highschool
*/

#include "configuration.h"
#include "state_machine.h"
#include "debug.h"
#include "IO.h"

#ifdef SIMULATION /*simulated hardware functions*/
  #include "simulated_arduino.h"
  #include "simulated_wrapper.h"
#endif 

void setup() {
  #ifndef SIMULATION 
    Serial.begin(115200);
  #endif

  Serial.println("Configuring outputs...\n");

  pinMode(C_START_BTN, INPUT_PULLUP);
  pinMode(C_STOP_BTN, INPUT_PULLUP);

  pinMode(C_PISTON_1_IN_1, OUTPUT);
  pinMode(C_PISTON_1_IN_2, OUTPUT);
  pinMode(C_PISTON_1_SPEED, OUTPUT);

  pinMode(C_PISTON_2_IN_1, OUTPUT);
  pinMode(C_PISTON_2_IN_2, OUTPUT);
  pinMode(C_PISTON_2_SPEED, OUTPUT);

  pinMode(C_ENC_1_A, INPUT);
  pinMode(C_ENC_1_B, INPUT);
  pinMode(C_ENC_2_A, INPUT);
  pinMode(C_ENC_2_B, INPUT);

  attachInterrupt(digitalPinToInterrupt(C_ENC_1_A), encoderOneInterrupt, CHANGE);
  attachInterrupt(digitalPinToInterrupt(C_ENC_1_B), encoderOneInterrupt, CHANGE);

  attachInterrupt(digitalPinToInterrupt(C_ENC_2_A), encoderTwoInterrupt, CHANGE);
  attachInterrupt(digitalPinToInterrupt(C_ENC_2_B), encoderTwoInterrupt, CHANGE);

  pinMode(C_VALVE_1, OUTPUT);
  pinMode(C_VALVE_2, OUTPUT);
  pinMode(C_VALVE_3, OUTPUT);
  pinMode(C_VALVE_4, OUTPUT);
  pinMode(C_VALVE_5, OUTPUT);
  pinMode(C_VALVE_6, OUTPUT);

  Serial.println("Outputs configured, setting default output values\n");

  digitalWrite(C_VALVE_1, HIGH);
  digitalWrite(C_VALVE_2, HIGH);
  digitalWrite(C_VALVE_3, HIGH);
  digitalWrite(C_VALVE_4, HIGH);
  digitalWrite(C_VALVE_5, HIGH);
  digitalWrite(C_VALVE_6, HIGH);

  Serial.println("End of setup..\n");
}

void loop() {
  state_machine_run();

}
