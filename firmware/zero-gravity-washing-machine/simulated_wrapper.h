/*
Arduino function prototypes and function alternatives
*/
#ifndef __SIMULATED_WRAPPER_H__
#define __SIMULATED_WRAPPER_H__

#include "state_machine.h"


enum PROCESS_RESULTS{
    SUCCESS,
    INVALID_INPUT,
    FAILURE
};

void setup(); /*Runs excatly once on startup*/
void loop(); /*Loops forever but only after setup() returns*/

int validateCMD(); /*returns non zero on invalid command*/
int setIO(); /*returns non zero on inability to set IO command*/
void visalizeOutput(); /*Prints Output's to commandline*/


#endif